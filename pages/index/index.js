// pages/index/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    avatarUrl:"../images/login.png",
    pd:"true",
    m:'0',
    name2:"个人设置",
    iconList: [{
      icon: 'recordfill',
      color: 'orange',
      badge: 1,
      name: '录像'
    }, {
      icon: 'picfill',
      color: 'yellow',
      badge: 0,
      name: '图像'
    }, {
      icon: 'questionfill',
      color: 'olive',
      badge: 22,
      name: '帮助'
    },
    {
      icon: 'repairfill',
      color: 'mauve',
      badge: 0,
      name: '设置'
    }, {
      icon: 'clothesfill',
      color: 'blue',
      badge: 0,
      name: '皮肤'
    },  {
      icon: 'brandfill',
      color: 'mauve',
      badge: 0,
      name: '关于'
    }],
    gridCol:4,
    skin: false
  
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },


  getUserProfile: function () {
    if(this.data.m==0){

    
    wx.getUserProfile({
      desc: '您的信息仅作为个人展示噢',
      success: (res) => {
        console.log('获取用户信息成功', res)
        wx.setStorage({
          data: res.userInfo,
          key: 'userInfo',
        });
        this.setData({
          m:1
        })
        this.onShow();
      }})
    
    }else{
      wx.showToast({
        title: '您已登录啦',
      })
    }
      
    },
  
  onMySet:function(e){
  if(e.currentTarget.dataset.type=="设置"){
    wx.navigateTo({
      url: '/pages/index/setting/setting',
    })
  }
  else if(e.currentTarget.dataset.type=="关于"){
    wx.navigateTo({
      url: '/pages/index/setting/about/about',
    })
  }
  
},

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {


  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (!wx.getStorageSync('useInfo')){
      this.setData({
        pd:true,
      })
    } else if(wx.getStorageSync('useInfo')){
      this.setData({
        pd:false,
    })
    
  }

    var useInfo = wx.getStorageSync('userInfo')
    var that = this;
    that.setData({
    avatarUrl:useInfo.avatarUrl,
    name:useInfo.nickName,
  })

  
    

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})