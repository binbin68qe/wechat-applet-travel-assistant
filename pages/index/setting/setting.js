// pages/index/setting/setting.js
const util = require('../../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    info:["头像","名称","收货地址","版本","关于"],
    tx:"",
    name:"",
    currentDate:util.formatTime(new Date(), '-', false),
    phone:""


  },
  getPhoneNumber (e) {
    console.log(e.detail.errMsg)
    console.log(e.detail.iv)
    console.log(e.detail.encryptedData)
  },
  bindDateChange: function (e) {
    console.log(e.detail.value)
    this.setData({
      currentDate:e.detail.value
    }) },
      chooseAddress() {
        wx.chooseAddress({
          success (res) {
            console.log(res.userName)
            console.log(res.postalCode)
            console.log(res.provinceName)
            console.log(res.cityName)
            console.log(res.countyName)
            console.log(res.detailInfo)
            console.log(res.nationalCode)
            console.log(res.telNumber)
          }
        })
    },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var useInfo = wx.getStorageSync('userInfo')
    var that = this;
    that.setData({
    tx:useInfo.avatarUrl,
    name:useInfo.nickName,})

  },

  houtai(){
    wx.navigateTo({
      url: '/pages/yanzhen/yanzhen',
    })

  },
  version(){
    wx.showToast({
      title: '当前已是最新版本~',
    })

  },
  logout:function() {
    wx.removeStorage({
      key: 'userInfo',
      success (res) {
        wx.showModal({
          title: '提示',
          content: '真的要退出了吗',
          cancelText:'取消',
          confirmText:'确定',
          confirmColor:'#576b95',
          cancelColor:'#000000',
          success (res) {
            if (res.confirm) {
              wx.reLaunch({
                url: '/pages/index/index',
              })
            } else if (res.cancel) {console.log('用户点击取消')}
          }
        })
        
      }
    })

  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})