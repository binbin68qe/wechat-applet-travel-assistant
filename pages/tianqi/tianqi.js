
//引入工具类
var getWeather = require("../../utils/getWeather.js");

Page({
  data: {
    wd: null, //从服务器返回的天气数据对象
    cityName: "", //城市名
    updateString1: "", //更新日期
    updateString2: "", //更新时间
    imageUrl: "", //天气图片url
    bgUrl:"", //背景图片url
  },

  //在onShow生命周期函数中获取天气数据
  onShow: function () {
    var that = this; // 用that局部变量保存当前页面this
    getWeather.getWeather(this.data.cityName, function (wd) {
      
      if (!wd.errcode) {
        for(var i=0;i<wd.data[0].hours.length;i++)
       {
      var weaImageUrl=that.getImageUrl(i,wd);
       wd.data[0].hours[i].imageUrl=weaImageUrl;
        }
      wd.data[0].week='今天';
        var strArray = wd.update_time.split(' ');
        that.setData({
          wd: wd,
          updateString1: strArray[0] + ' ' + wd.data[0].week,
          updateString2: strArray[1] + "更新",
          imageUrl:that.getImageUrl(-1,wd),
          bgUrl:that.getBgUrl(wd),
          aaaa:that.getImageUrl(1,wd)
        });
        console.log(that.data.wd);
      } else {

        that.setData({
          wd: wd,
        });
      }
      
    });
    
  
  
  },

  onLoad: function (option) {
    this.setData({
      cityName:option.cityName
    })
    
  },
  //点击选择城市时切换到city页面
  selectCity: function () {
    console.log("selectCity");
    wx.navigateTo({
      url: '/pages/city/city',
    })
  },
  //获取天气信息图片路径
  getImageUrl: function (i,wd) {
    var wea = wd.data[0].wea;
    var url = "";
    var hour;
    if(i<0){
      var date = new Date();
      hour = date.getHours();
    }
    else{
      hour = parseInt(wd.data[0].hours[i].hours);
    }

    var isDay = this.getIsDay(hour,wd.data[0].sunset,wd.data[0].sunrise);

    if (wea.search("晴") != -1) {
      if (isDay) {
        url = "/images/wea/tq_qing.png";  
      } else {
        url = "/images/wea/tq_qing_night.png";
      }
    } else if (wea.search("云") != -1) {
      if (isDay) {
        url = "/images/wea/tq_duoyun.png";
      } else {
        url = "/images/wea/tq_duoyun_night.png";
      }
    } else if (wea.search("阴") != -1) {
      url = "/images/wea/tq_yin.png";
    } else if (wea.search("暴雨") != -1) {
      url = "/images/wea/tq_baoyu.png";
    } else if (wea.search("大雨") != -1) {
      url = "/images/wea/tq_dayu.png";
    } else if (wea.search("中雨") != -1) {
      url = "/images/wea/tq_zhongyu.png";
    } else if (wea.search("小雨") != -1) {
      url = "/images/wea/tq_xiaoyu.png";
    } else if (wea.search("雷阵雨") != -1) {
      url = "/images/wea/tq_leizhenyu.png";
    }
    return url;
  },

   //获取背景url路径
  getBgUrl:function(wd){
    var date = new Date();
    var hour = date.getHours();
    var isDay = this.getIsDay(hour,wd.data[0].sunset,wd.data[0].sunrise);
    var bgUrl = "";
    bgUrl=isDay?"https://wxabc.xyz/bg/bg_"+wd.data[0].wea_img+".jpg":
    "https://wxabc.xyz/bg/bg_"+wd.data[0].wea_img+"_night.jpg";
    return bgUrl;
  },


  //获取是否为白天，true为白天，false为晚上,sunset太阳升起时间，sunrise落山时间
  getIsDay:function(hour,sunset,sunrise)
  { 
    var timeArray1 = sunset.split(':');
    var timeArray2 = sunrise.split(':');
    var isDay = true;
    //当前时间大于太阳落山时间或者当前时间小于太阳升起时间
    if (hour > parseInt(timeArray1[0]) || hour < parseInt(timeArray2[0])) {
      isDay = false
    }
    return isDay;
  }
})
