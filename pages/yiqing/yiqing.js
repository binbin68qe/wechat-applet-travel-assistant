// pages/yiqing/yiqing.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cityname:'',
    code:'',
    list:[],
    swiperList: [ {
      id: 0,
        type: 'image',
        url: '/pages/images/2.png',
    }, {
      id: 1,
      type: 'image',
      url: '/pages/images/3.jpg'
    },  {
      id: 2,
      type: 'image',
      url: '/pages/images/5.jpg'
    }, {
      id: 3,
      type: 'image',
      url: '/pages/images/6.jpg'
    }, ],

  },
  listenerCityInput: function (e) {
    this.data.cityname=e.detail.value;
  },
  searchDataByName(){
    var cityname = this.data.cityname;
    console.log(cityname)
    this.setData({
      cityname:cityname,
    })
       
    wx.navigateTo({
      url: '/pages/tianqi/tianqi?cityName='+cityname
    })
   

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})