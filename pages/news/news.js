//最初加载页面时的url

var url = 'https://v.juhe.cn/toutiao/index?type=shishang&key=e01fe8c6572b93da3253edf71d0e5035&is_filter=1&page=1'

var colors = {
  shishng: '#BDC6B8',
      jiankang: '#BDC6B8',
      yule: '#BDC6B8',
      tiyu: '#BDC6B8',
      junshi: '#BDC6B8',
      keji: '#BDC6B8',
      caijin: '#BDC6B8',
      qiche: '#BDC6B8'
    }

//点击某类新闻时导航栏上对应的字变为红色
var changeColor = function (curColor){
    for (var i in colors){
      if (i == curColor){
        colors[i] = 'red';
      }else{
        colors[i] = '#BDC6B8';
      }
    }
  }

Page({
  data:{
    cityname:'',
    newslist:[],
    page: 1,
    newstype: '',
    scrollHeight: 0,
    hidden: true,
    color: {
      shishang: 'red',
      jiankang: '#BDC6B8',
      yule: '#BDC6B8',
      edu: '#BDC6B8',
      ent: '#BDC6B8',
      money: '#BDC6B8',
      gupiao: '#BDC6B8',
      travel: '#BDC6B8'
    }
  },

  // 页面初始化 options为页面跳转所带来的参数
  onLoad:function(options){
    var _this = this;
   this.setData({
      hidden: false, // 阴藏或显示加载更多
    });

   // 网络请求
   wx.request({
      url: url,
      method: 'post',
      success: (res) => {
        _this.setData({
            newslist: res.data.result.data,
            hidden: true
        });
      }
    });
    //获得窗口的高度，在划到页面最底部时加载更多要用
    wx.getSystemInfo({
      success: function(res) {
        _this.setData({
          scrollHeight: res.windowHeight
        });
      }
    });
  },

  //浏览某条新闻
  browsing: function(event){
    var newstype = event.currentTarget.id;
    changeColor(newstype);
    this.setData({
      newstype: newstype,
      color: colors
     
    });
    url ='https://v.juhe.cn/toutiao/index?type='+newstype+'&key=e01fe8c6572b93da3253edf71d0e5035&page=1&is_filter=1';
    wx.request({
      url: url,
      method: 'post',
      success: (res) => {
        this.setData({
            newslist: res.data.result.data,
            page: 1
        });
      }
    });
  },

  //下拉或上拉加载更多
  loadmore: function (event){
    this.setData({
      hidden: false,
      page: this.data.page + 1
    });
    url = 'https://v.juhe.cn/toutiao/index?type='+this.data.newstype+'&key=e01fe8c6572b93da3253edf71d0e5035'+'&page='+ this.data.page+'&is_filter=1';
    wx.request({
      url: url,
      method: 'post',
      success: (res) => {
        this.setData({
            newslist: res.data.list,
            hidden: true
        });
      }
    })
  }
})