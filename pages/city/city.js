// pages/city/city.js
Page({
  data: {
    cityName: "",
    rmcity:["北京","上海","广州","深圳","成都","重庆","天津","杭州","南京","苏州"],
  },
  searchWeather: function () {
    //当前页面
    var pages = getCurrentPages();
    //上一页面
    var prevPage = pages[pages.length - 2];
    //将数值信息赋值给上一页面cityItem变量
    prevPage.setData({
      cityName: this.data.cityName
    });

    //关闭当前界面返回到上一页面
    wx.navigateBack({
      //返回栈数
      delta: 1
    })
  },
  //当输入框输入数据时，更新cityName数据。
  getInputValue(e){
    this.setData({
      cityName:e.detail.value
    })
  }
})
