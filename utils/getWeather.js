/*获取天气数据*/
/*
    创建方法，用来根据城市获取天气信息 
    参数：
        cityName 城市名    
        callback 回调函数返回数据 
*/
function getWeather(cityName, callback) {

  //判断cityName是否存在
  if (!cityName) {
    //如果不存在，则使用空字符串默认获取当前ip所在城市
    cityName = ""; 
  }

  //调用天气API查询气象信息
  var weApi = "https://tianqiapi.com/api";

  //发送请求加载信息
  wx.request({
    url: weApi,
    data: {
      version:"v1",
      appid:"26373119", //填写你注册的appid
      appsecret:"6UVah6jX ", //填写你注册的appsecret
      city: cityName,
    },

    success: function (res) {
      console.log(res.data);
      //加载成功，获取需要的数据，调用回调函数发回信息
      callback && callback(res.data);
    }
  })
}

//设置导出信息
module.exports = {
  getWeather: getWeather
};

