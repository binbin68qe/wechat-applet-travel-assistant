# 微信小程序出行助手

#### 介绍
可以查看天气 搜索地方天气，查看综合类新闻，疫情数据，综合新闻视频等

![项目截图](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-9de17fea-2142-4abf-ac2c-77c7395060eb/fff6d8c0-c43d-4059-864b-f90d43e1f0ab.jpg)
![项目截图](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-9de17fea-2142-4abf-ac2c-77c7395060eb/cc4c12e4-603d-4db6-8c54-3f13d9b299e5.jpg)
#### 安装教程
- 导入小程序项目选择包含`project.config.json`的目录，并选择自己的小程序AppID;
- 修改`/miniprogram/app.js`中的 `env: '你的云开发环境id'`等相关参数;
- 部署完成;